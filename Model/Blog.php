<?php
namespace TestProject\Model;

class Blog
{
    protected $oDb;

    public function __construct()
    {
        $this->oDb = new \TestProject\Engine\Db;
    }

    public function get($iOffset, $iLimit)
    {
        $oStmt = $this->oDb->prepare('SELECT * FROM Posts ORDER BY createdDate DESC LIMIT :offset, :limit');
        $oStmt->bindParam(':offset', $iOffset, \PDO::PARAM_INT);
        $oStmt->bindParam(':limit', $iLimit, \PDO::PARAM_INT);
        $oStmt->execute();
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getAll()
    {
        $oStmt = $this->oDb->query('SELECT * FROM Posts ORDER BY createdDate DESC');
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function getHashtagByPostId($iId)
    {
        $iId = intval($iId);
        $oStmt = $this->oDb->query("SELECT * FROM hashtag WHERE post_id = '$iId' LIMIT 1");
        return $oStmt->fetchAll(\PDO::FETCH_OBJ);
    }

    public function add()
    {
        $aTitle = $_POST['title'];
        $aBody = $_POST['body'];
        $sCreated_date = date('Y-m-d H:i:s');

        $imgFile = $_FILES['image']['name'];
        $tmp_dir = $_FILES['image']['tmp_name'];
        $imgSize = $_FILES['image']['size'];
        $imgExt = strtolower(pathinfo($imgFile, PATHINFO_EXTENSION));
        $validExtensions = array('jpeg', 'jpg', 'png');
        $postImage = rand(1000, 1000000).".".$imgExt;
        if (in_array($imgExt, $validExtensions)) {
            if ($imgSize < 5000000) {
                move_uploaded_file($tmp_dir, IMAGE_PATH.$postImage);
            }
        } else {
            echo "Only jpeg png jpg";
        }

        $audioFile =   $_FILES['audio']['name'];
        move_uploaded_file($_FILES["audio"]["tmp_name"],"audio/" . $_FILES["audio"]["name"]);



        $oStmt = $this->oDb->prepare('INSERT INTO Posts (title, body, image, audio, createdDate) VALUES(:title, :body, :image, :audio, :created_date)');
        $oStmt->bindParam(':title', $aTitle);
        $oStmt->bindParam(':body', $aBody);
        $oStmt->bindParam(':image', $postImage);
        $oStmt->bindParam(':audio', $audioFile);
        $oStmt->bindParam(':created_date', $sCreated_date);
        return $oStmt->execute();
    }

    public function getById($iId)
    {
        $oStmt = $this->oDb->prepare('SELECT * FROM Posts WHERE id = :postId LIMIT 1');
        $oStmt->bindParam(':postId', $iId, \PDO::PARAM_INT);
        $oStmt->execute();
        return $oStmt->fetch(\PDO::FETCH_OBJ);
    }

    public function update(array $aData)
    {
        $imgFile = $_FILES['image']['name'];
        $tmp_dir = $_FILES['image']['tmp_name'];
        $imgSize = $_FILES['image']['size'];

        $iId = $_GET['id'];
        $oStmt_edit = $this->oDb->prepare('SELECT * FROM Posts WHERE id =:postId');
        $oStmt_edit -> execute(array('postId' => $iId));
        $aRow = $oStmt_edit->fetch(\PDO::FETCH_ASSOC);
        extract($aRow);

        $upload_dir = 'image/'; // upload directory
        $imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION)); // get image extension
        $valid_extensions = array('jpeg', 'jpg', 'png', 'gif'); // valid extensions
        $userpic = rand(1000,1000000).".".$imgExt;
        if(in_array($imgExt, $valid_extensions))
        {
            if($imgSize < 5000000)
            {
                unlink($upload_dir.$aRow['image']);
                move_uploaded_file($tmp_dir,$upload_dir.$userpic);
            }
            else
            {
                $errMSG = "Sorry, your file is too large it should be less then 5MB";
            }
        }
        else
        {
            $errMSG = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        }
        if (empty($imgFile)) {
            $userpic = $aRow['image'];
        } else {
            echo "image not empty!!";
        }

        $audioFile =   $_FILES['audio']['name'];

        $oStmt_editAudio = $this->oDb->prepare('SELECT * FROM Posts WHERE id =:postId');
        $oStmt_editAudio -> execute(array('postId' => $iId));
        $aRowAudio = $oStmt_editAudio->fetch(\PDO::FETCH_ASSOC);
        extract($aRowAudio);

        if (empty($audioFile)) {
            $audioFile = $aRowAudio['audio'];
        } else {
            $sAudioDir = "audio/";
            unlink($sAudioDir . $aRowAudio['audio']);
            move_uploaded_file($_FILES["audio"]["tmp_name"], "audio/" . $_FILES["audio"]["name"]);
        }

        $oStmt = $this->oDb->prepare('UPDATE Posts SET title = :title, body = :body, image = :image, audio = :audio WHERE id = :postId LIMIT 1');
        $oStmt->bindValue(':postId', $aData['post_id'], \PDO::PARAM_INT);
        $oStmt->bindValue(':title', $aData['title']);
        $oStmt->bindValue(':body', $aData['body']);
        $oStmt->bindValue(':image', $userpic);
        $oStmt->bindValue(':audio', $audioFile);
        return $oStmt->execute();

    }

    public function delete($iId)
    {
        $oStmt = $this->oDb->prepare('SELECT image FROM Posts WHERE id =:postId');
        $oStmt->execute(array(':postId'=>$_GET['id']));
        $imgRow = $oStmt->fetch(\PDO::FETCH_ASSOC);
        unlink("image/".$imgRow['image']);

        $oStmtAudio = $this->oDb->prepare('SELECT audio FROM Posts WHERE id =:postId');
        $oStmtAudio->execute(array(':postId'=>$_GET['id']));
        $aAudioRow = $oStmtAudio->fetch(\PDO::FETCH_ASSOC);
        unlink("audio/".$aAudioRow['audio']);

        $oStmtAudio = $this->oDb->prepare('DELETE FROM Posts WHERE id = :postId LIMIT 1');
        $oStmtAudio->bindParam(':postId', $iId, \PDO::PARAM_INT);

        $oStmt = $this->oDb->prepare('DELETE FROM Posts WHERE id = :postId LIMIT 1');
        $oStmt->bindParam(':postId', $iId, \PDO::PARAM_INT);
        return $oStmt->execute() && $oStmtAudio->execute() ;
    }

}
