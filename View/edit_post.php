
<?php require 'inc/header.php' ?>
<?php require 'inc/msg.php' ?>

<?php if (empty($this->oPost)): ?>
    <p class="error">Post Data Not Found!</p>
<?php else: ?>
<div class="container">
    <form action="" method="post" enctype="multipart/form-data">
        <p><label for="title">Title:</label><br />
            <input type="text" name="title" id="title" value="<?=htmlspecialchars($this->oPost->title)?>" required="required" />
        </p>

        <p><label for="body">Body:</label><br />
            <textarea name="body" id="body" rows="5" cols="35" required="required"><?=htmlspecialchars($this->oPost->body)?></textarea>
        </p>
        <img src="image/<?php echo $this->oPost->image ?>" alt="" width="200" height="200">
        <input type="hidden" name="size" value="16">
        <input type="file" name="image" id="image" accept="image/*">
        <audio src="audio/<?php echo $this->oPost->audio?>" preload="none"></audio>
        <input type="file" name="audio" id="audio" accept="audio/*">

        <p>Upload Video
            <input type="file" name="video" id="video" accept="video/*">
        </p>

        <p><input type="submit" name="edit_submit" value="Update" /></p>
    </form>
    <div>
<?php endif ?>

<?php require 'inc/footer.php' ?>
