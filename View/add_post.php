<?php require 'inc/header.php' ?>
<?php require 'inc/msg.php' ?>
<div class="container">
<form action="" method="post" enctype="multipart/form-data">

    <p>
        <label for="title">Название :</label><br />
        <input type="text" name="title" id="title" required="required" />
    </p>

    <p>
        <label for="body">Описание :</label><br />
        <textarea name="body" id="body" rows="5" cols="35" required="required"></textarea>
    </p>

    <input type="hidden" name="size" value="16">
    <input type="file" name="image" id="image" accept="image/*">
    <input type="file" name="audio" id="audio" accept="audio/*">


    <p><input type="submit" name="add_submit" value="Add" /></p>
</form>
</div>
<?php require 'inc/footer.php' ?>
