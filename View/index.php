<?php require 'inc/header.php' ?>


    <?php if (empty($this->oPosts)): ?>
        <p xmlns="http://www.w3.org/1999/html">Тут нет записей</p>
    <p><button type="button" onclick="window.location='<?=ROOT_URL?>?p=blog&amp;a=add'" >Добавь свой первый запись!</button></p>
<?php else: ?>
<?php require 'inc/breadcrumb.php' ?>

<div class="row">



                <!-- Blog Entries Column -->
                <div class="col-md-8">

                    <?php foreach ($this->oPosts as $oPost): ?>
                    <!-- First Blog Post -->
                    <h2>
                        <a href="<?=ROOT_URL?>?p=blog&amp;a=post&amp;id=<?=$oPost->id?>"><?=htmlspecialchars($oPost->title)?></a>
                    </h2>
                    <p class="lead">
                        by <a href="index.php">Start Bootstrap</a>
                    </p>

                    <p   class="alert-dismissible"><i class="fa fa-clock-o"></i>  Posted on <?=$oPost->createdDate?></p>

                        <hr>
                    <p><?=nl2br(htmlspecialchars(mb_strimwidth($oPost->body, 0, 100, '...')))?></p>

                    <a>
                        <img class="img-responsive img-hover" src="image/<?php echo $oPost->image ?>" alt="">
                    </a>

                    <audio src="audio/<?php echo $oPost->audio?>" preload="none"></audio>

                    <p><a  class="btn btn-primary" href="<?=ROOT_URL?>?p=blog&amp;a=post&amp;id=<?=$oPost->id?>">Want to see more?<i class="fa fa-angle-right"></i></a></p>
                        <hr>

                    <?php require 'inc/control_buttons.php' ?>

                    <?php endforeach ?>

                    <?php endif ?>






                    <!-- Pager -->
                    <ul class="pager">
                        <li class="previous">
                            <a href="#">&larr; Older</a>
                        </li>
                        <li class="next">
                            <a href="#">Newer &rarr;</a>
                        </li>
                    </ul>


                </div>


                <!-- Blog Sidebar Widgets Column -->

    <?php require 'inc/category.php' ?>
            </div>
            <!-- /.row -->

            <hr>









<?php require 'inc/footer.php' ?>
