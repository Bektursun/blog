<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title><?=\TestProject\Engine\Config::SITE_NAME?></title>
        <meta name="author" content="Brothers" />
        <link rel="stylesheet" href="<?=ROOT_URL?>static/css/style.css" />
        <link rel="stylesheet" href="<?=ROOT_URL?>static/css/normalize.css" />
        <link rel="stylesheet" href="<?=ROOT_URL?>static/css/font-awesome.css" />
        <link rel="stylesheet" href="<?=ROOT_URL?>static/css/bootstrap.css" />
        <script src="<?=ROOT_URL?>static/js/jquery.js"> </script>
        <script src="<?=ROOT_URL?>static/js/bootstrap.js"> </script>
        <script src="<?=ROOT_URL?>static/js/jquery.jplayer.min.js"> </script>
        <script src="<?=ROOT_URL?>static/js/index.js"> </script>
        <script src="<?=ROOT_URL?>audiojs/audio.min.js"></script>


    </head>
    <body>
                    <script>
                        audiojs.events.ready(function () {
                            var as = audiojs.createAll();
                        });

                        $(document).ready(function () {
                            document.addEventListener('play', function (e) {
                                var audios = document.getElementsByTagName('audio');
                                for (var i = 0, len = audios.length; i < len; i++) {
                                    if (audios[i] != e.target) {
                                        audios[i].pause();
                                    }
                                }
                            }, true);
                        })
                    </script>

                    <!-- Navigation -->
                    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                        <div class="container">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="index.html">Brothers Blog</a>
                            </div>
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav navbar-right">
                                    <li>
                                        <a href="#">About</a>
                                    </li>
                                    <li>
                                        <a href="#">Services</a>
                                    </li>
                                    <li>
                                        <a href="#">Contact</a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Portfolio <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="#">1 Column Portfolio</a>
                                            </li>
                                            <li>
                                                <a href="#">2 Column Portfolio</a>
                                            </li>
                                            <li>
                                                <a href="#">3 Column Portfolio</a>
                                            </li>
                                            <li>
                                                <a href="#">4 Column Portfolio</a>
                                            </li>
                                            <li>
                                                <a href="#">Single Portfolio Item</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown active">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li class="active">
                                                <a href="#">Blog Home 1</a>
                                            </li>
                                            <li>
                                                <a href="#">Blog Home 2</a>
                                            </li>
                                            <li>
                                                <a href="#">Blog Post</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle." data-toggle="dropdown">Other Pages <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="#">Full Width Page</a>
                                            </li>
                                            <li>
                                                <a href="#">Sidebar Page</a>
                                            </li>
                                            <li>
                                                <a href="#">FAQ</a>
                                            </li>
                                            <li>
                                                <a href="#">404</a>
                                            </li>
                                            <li>
                                                <a href="#">Pricing Table</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.navbar-collapse -->
                        </div>
                        <!-- /.container -->
                    </nav>

                    <!-- Page Content -->
                    <div class="container">
                        0


