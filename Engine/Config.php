<?php

namespace TestProject\Engine;

final class Config
{

    // Database info (if you want to test the script, please edit the below constants with yours)
    const
    DB_HOST = 'localhost',
    DB_NAME = 'blog',
    DB_USR = 'root',
    DB_PWD = '12345',

    // Title of the site
    SITE_NAME = 'My Simple Blog!';

}
