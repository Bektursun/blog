


<h1><a href="<?=ROOT_URL?>?p=blog&amp;a=post&amp;id=<?=$oPost->id?>"><?=htmlspecialchars($oPost->title)?></a></h1>
<p><?=nl2br(htmlspecialchars(mb_strimwidth($oPost->body, 0, 100, '...')))?></p>
<img src="image/<?php echo $oPost->image ?>">
<audio src="audio/<?php echo $oPost->audio?>" preload="none"></audio>

<p><a  class="btn btn-primary" href="<?=ROOT_URL?>?p=blog&amp;a=post&amp;id=<?=$oPost->id?>">Want to see more?<i class="fa fa-angle-right"></i></a></p>
<p><i class="fa fa-clock-o"></i>Posted on <?=$oPost->createdDate?></p>

<?php require 'inc/control_buttons.php' ?>
